package com.dvo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Danyl on 001 01.08.17.
 */
public class FindPrime {
    int a;
    int b;

    public List<Integer> inputEntry(String a, String b){
        this.a = Integer.parseInt(a);
        this.b = Integer.parseInt(b);
        if(this.a < 2 || this.b < 2){
            throw new ArithmeticException();
        }
        if(this.a >= this.b){
            throw new LogicalException();
        }
        return findPrime(this.a,this.b);
    }

     private List<Integer> findPrime(int a, int b){
         boolean[] grid = new boolean[b];
         List<Integer> result = new ArrayList<>();
         for (int i = 0; i < b; i++) {
             grid[i] = true;
         }
         for (int i = 2; i*i < b; i++) {
            if(grid[i] == true){
                for (int j = i*i; j < b; j+=i) {
                    grid[j] = false;
                }
            }
         }
         for (int i = a; i < b; i++) {
             if (grid[i]) {
                result.add(i);
             }
         }
         return result;
     }


}
