package com.dvo;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String a;
        String b;
        System.out.println("Enter lower border \"a\": ");
        a = scanner.next();
        System.out.println("Enter upper border \"b\": ");
        b = scanner.next();
        FindPrime findPrime = new FindPrime();
        for (Integer x:findPrime.inputEntry(a, b)) {
            System.out.println(x);
        }
    }
}
