import static org.junit.Assert.*;

import com.dvo.FindPrime;
import com.dvo.LogicalException;
import org.junit.*;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Danyl on 001 01.08.17.
 */
public class testPrime {
    FindPrime findPrime = new FindPrime();
    int[] res = {0, 0};
    int[]checker = {1, 3};

    @Test(expected = NumberFormatException.class)
    public void testIsNumberNegatve(){
        findPrime.inputEntry("A","1");
        findPrime.inputEntry("1","A");
    }


    @Test(expected = ArithmeticException.class)
    public void testIsEntryPositiveFalse(){
        findPrime.inputEntry("-1","1");
        findPrime.inputEntry("1","-1");
    }

    @Test(expected = LogicalException.class)
    public void testIsBGreaterAFalse(){
        findPrime.inputEntry("5","2");
    }

    @Test
    public void testForCorrectFiltering(){
        List<Integer> toCheck;
        List<Integer> checked = new ArrayList<>();
        checked.add(5);
        checked.add(7);
        toCheck = findPrime.inputEntry("5","10");
        assertTrue(checked.equals(toCheck));
    }

}
